FROM node:latest as builder

WORKDIR /app
RUN git clone https://github.com/Dreamacro/clash-dashboard.git .
RUN yarn && yarn run build


FROM alpine:latest
COPY --from=builder /app/dist /www
RUN apk update && apk add mini_httpd
EXPOSE 80
ENTRYPOINT ["mini_httpd", "-d", "/www", "-p", "80", "-D"]

